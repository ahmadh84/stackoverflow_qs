Repository containing code for questions I post on StackOverflow or other public websites
=========================================================================================

### ffmpeg_jpeg ###
This repository shows that FFmpeg mjpeg decoding results in inconsistent values from the original frames.

Detailed question description on StackOverflow
[http://stackoverflow.com/questions/41341766](http://stackoverflow.com/questions/41341766)

Build: 
```
mkdir build
cd build
cmake ..
make
```

Options:

* Help: `./ffmpegjpg --help`
* N frames to read: `./ffmpegjpg --endframe [N-1]`
* png decoding: `./ffmpegjpg --filetype png`