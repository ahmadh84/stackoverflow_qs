#include <iostream>
#include <boost/program_options.hpp>

namespace po = boost::program_options;

#define cimg_use_png
#define cimg_use_jpeg
#define cimg_use_tiff
#include "CImg.h"

extern "C" {
    #include <libavcodec/avcodec.h>
    #include <libavformat/avformat.h>
    #include <libswscale/swscale.h>
    #include <libavdevice/avdevice.h>
    #include <libavutil/imgutils.h>
}


void pckd_avframe_to_arr(const AVFrame* const avframe, const unsigned int& height, 
                         const unsigned int& width, unsigned char* t_ptr)
{
    unsigned char* g_ptr = t_ptr + (height * width * sizeof(unsigned char));
    unsigned char* b_ptr = g_ptr + (height * width * sizeof(unsigned char));
    unsigned int pxl_i = 0;

    for (unsigned int r = 0; r < height; ++r) {
        const uint8_t* avframe_r = avframe->data[0] + r*avframe->linesize[0];
        for (unsigned int c = 0; c < width; ++c) {
            t_ptr[pxl_i] = avframe_r[0];
            g_ptr[pxl_i] = avframe_r[1];
            b_ptr[pxl_i] = avframe_r[2];
            avframe_r += 3;
            ++pxl_i;
        }
    }
}

int load_ffmpeg_vid(const std::string vid_filepattern, 
                    const unsigned int read_nframes, 
                    unsigned char*& output_arr)
{
    AVFormatContext* fmt_ctx = NULL;
    AVCodecContext* cdc_ctx = NULL;
    AVCodec* vid_cdc = NULL;
    int ret;

    /* register codecs and formats and other lavf/lavc components*/
    av_register_all();
    
    unsigned int height, width;

    /* open input file, and allocate format context */
    if (avformat_open_input(&fmt_ctx, vid_filepattern.c_str(), NULL, NULL) != 0) {
        std::cerr << "Could not open video: " << vid_filepattern << std::endl;
    	return -1;
    }

    /* retrieve stream information */
    if (avformat_find_stream_info(fmt_ctx, NULL) < 0) {
    	std::cerr << "Could not stream information: " << vid_filepattern << std::endl;
    	return -2;
    }

    /* Dump information about file onto standard error */
    // av_dump_format(fmt_ctx, 0, vid_filepattern.c_str(), 0);

    int video_stream_idx = av_find_best_stream(fmt_ctx, AVMEDIA_TYPE_VIDEO, 
                                                -1, -1, NULL, 0);
    if (video_stream_idx < 0) {
    	std::cerr << "No " << av_get_media_type_string(AVMEDIA_TYPE_VIDEO) 
                  << " streams found: " << vid_filepattern << std::endl;
    	return -3;
    }

    AVCodecParameters* cdc_params = fmt_ctx->streams[video_stream_idx]->codecpar;

    vid_cdc = avcodec_find_decoder(cdc_params->codec_id);
    if (vid_cdc == NULL) {
        std::cerr << "Unsupported codec: " << vid_filepattern << std::endl;
        return -4;
    }

    cdc_ctx = avcodec_alloc_context3(vid_cdc);
    if (cdc_ctx == NULL) {
    	std::cerr << "Could not allocate video codec context: " 
                  << vid_filepattern << std::endl;
    	return -5;
    }

    /* Copy codec parameters from input stream to output cdc_ctx */
    ret = avcodec_parameters_to_context(cdc_ctx, cdc_params);
    if (ret < 0) {
    	std::cerr << "Failed to copy " << av_get_media_type_string(AVMEDIA_TYPE_VIDEO) 
                  << " codec parameters to decoder context: " << vid_filepattern 
                  << std::endl;
    	return -6;
    }

    // std::cout << cdc_ctx->width << "x" << cdc_ctx->height << std::endl;
    width = cdc_ctx->width;
    height = cdc_ctx->height;
    output_arr = new unsigned char [height * width * 3 * sizeof(unsigned char) * read_nframes];

    /* allocate output buffer */

    ret = avcodec_open2(cdc_ctx, vid_cdc, NULL);
    if (ret < 0) {
    	std::cerr << "Could not open codec: " << vid_filepattern << std::endl;
    	return -7;
    }

    int num_bytes;
    uint8_t* buffer = NULL;
    const AVPixelFormat out_format = AV_PIX_FMT_RGB24;

    num_bytes = av_image_get_buffer_size(out_format, cdc_ctx->width, cdc_ctx->height, 1);  // avpicture_get_size  deprecated
    buffer = (uint8_t*)av_malloc(num_bytes * sizeof(uint8_t));

    AVFrame* b_vid_frame = NULL;
    b_vid_frame = av_frame_alloc();
    AVFrame* b_conv_frame = NULL;
    b_conv_frame = av_frame_alloc();
    if (b_vid_frame == NULL || b_conv_frame == NULL) {
    	std::cerr << "Frame alloc failed: " << vid_filepattern << std::endl;
    	return -8;
    }

    // avpicture_fill((AVPicture*)b_conv_frame , buffer , AV_PIX_FMT_GBRP , cdc_ctx->width , cdc_ctx->height);
    av_image_fill_arrays(b_conv_frame->data, b_conv_frame->linesize, buffer, 
                         out_format, cdc_ctx->width, cdc_ctx->height, 1);

    struct SwsContext *sws_ctx = NULL;
    sws_ctx = sws_getContext(cdc_ctx->width,
                             cdc_ctx->height,
                             cdc_ctx->pix_fmt,
                             cdc_ctx->width,
                             cdc_ctx->height,
                             out_format,
                             SWS_BILINEAR,
                             NULL,NULL,NULL);

    int frame_num = 0;
    AVPacket b_vid_pckt;
    while (av_read_frame(fmt_ctx, &b_vid_pckt) >=0) {
        ret = avcodec_send_packet(cdc_ctx, &b_vid_pckt);
        // In particular, we don't expect AVERROR(EAGAIN), because we read all
        // decoded frames with avcodec_receive_frame() until done.
        if (ret < 0)
            break;

        ret = avcodec_receive_frame(cdc_ctx, b_vid_frame);
        if (ret < 0 && ret != AVERROR(EAGAIN) && ret != AVERROR_EOF)
            break;
        if (ret >= 0) {
            // convert image from native format to planar GBR
            sws_scale(sws_ctx, b_vid_frame->data, 
                      b_vid_frame->linesize, 0, b_vid_frame->height, 
                      b_conv_frame->data, b_conv_frame->linesize);

            unsigned char* out_ptr = output_arr + 
                (height * width * sizeof(unsigned char) * 3 * frame_num);

            pckd_avframe_to_arr(b_conv_frame, height, width, out_ptr);

            ++frame_num;

            if (frame_num >= read_nframes)
                break;
        }
    }

    av_free(buffer);
    av_free(b_conv_frame);
    av_free(b_vid_frame);
    avcodec_close(cdc_ctx);
    avformat_close_input(&fmt_ctx);

    return height * width * 3 * read_nframes;
}

static int load_cimg_im(const std::string vid_filepattern, 
                        const unsigned int start_frame, const unsigned int end_frame, 
                        unsigned char*& output_arr)
{
    unsigned int height, width;
    char fp_buffer[500];

    const unsigned int read_nframes = end_frame - start_frame + 1;

    for (unsigned int frame_num = start_frame; frame_num <= end_frame; ++frame_num) {
        std::sprintf(fp_buffer, vid_filepattern.c_str(), frame_num);
        cimg_library::CImg<unsigned char> im(fp_buffer);

        // if memory not assigned
        if (!output_arr) {
            height = im._height;
            width = im._width;
            output_arr = new unsigned char [height * width * 3 * 
                                            sizeof(unsigned char) * read_nframes];
        }

        unsigned char* out_ptr = output_arr + 
            (height * width * sizeof(unsigned char) * 3 * frame_num);

        std::memcpy(out_ptr, im.data(), height * width * sizeof(unsigned char) * 3);
    }

    return height * width * 3 * read_nframes;
}

int main(int argc, char* argv[])
{
    po::options_description desc("Allowed options");
    desc.add_options()
        ("help", "produce a help message")
        ("endframe", po::value<int>()->default_value(15), "end frame number")
        ("filetype", po::value<std::string>()->default_value("JPEG"), "filetype - JPEG or png")
    ;

    try {
        po::variables_map opts;
        po::store(po::parse_command_line(argc, argv, desc), opts);

        if (opts.count("help")) {
          std::cerr << desc << std::endl;
          return 0;
        }

        po::notify(opts);

        const std::string FILE_TYPE = opts["filetype"].as<std::string>();
        const std::string DEST_DIR = "../../data/test_frames_" + FILE_TYPE + "/";
        const std::string DEST_FRAMES = DEST_DIR + "%06d." + FILE_TYPE;
        const std::string DEST_VID = DEST_DIR + "vid.avi";
        //ffmpeg -y -start_number 0 -i %06d.JPEG -codec copy vid.avi
        const unsigned int START_FRAME_NUM = 0;
        const unsigned int END_FRAME_NUM = opts["endframe"].as<int>();

        unsigned char* ffmpeg_out_arr = NULL;
        unsigned char* cimg_out_arr = NULL;
        unsigned int nvals;

        nvals = load_ffmpeg_vid(DEST_VID, END_FRAME_NUM - START_FRAME_NUM + 1, ffmpeg_out_arr);
        if (nvals >= 0) {
            std::cout << "ffmpeg reading success" <<std::endl;
        }

        if (load_cimg_im(DEST_FRAMES, START_FRAME_NUM, END_FRAME_NUM, cimg_out_arr) >= 0) {
            std::cout << "cimg reading success" <<std::endl;
        }

        // compare
        if (ffmpeg_out_arr && cimg_out_arr) {
            std::cout << "Comparing" << std::endl;
            unsigned int diff_cnt = 0;
            double total_diff = 0;
            for (unsigned int i = 0; i < nvals; ++i) {
                if (ffmpeg_out_arr[i] != cimg_out_arr[i]) {
                    ++diff_cnt;
                    total_diff += abs((double)ffmpeg_out_arr[i] - (double)cimg_out_arr[i]);
                }
            }
            std::cout << "Total vals diff: " << diff_cnt << " (" 
                      << ((double)diff_cnt/nvals * 100) << "%%)" << std::endl;
            std::cout << "Avg. difference: " << total_diff/diff_cnt << std::endl;
        }

        if (ffmpeg_out_arr) {
            delete [] ffmpeg_out_arr;
        }
        if (cimg_out_arr) {
            delete [] cimg_out_arr;
        }
    } catch (po::error& e) {
        std::cerr << "Program Options error: " << e.what() << std::endl;
        std::cerr << desc << std::endl;
        return -1;
    } catch (std::exception& e) {
        std::cerr << "Error: " << e.what() << std::endl;
        return -1;
    } catch (...) {
        std::cerr << "Unknown error!" << std::endl;
        return -1;
    }

	return 0;
}